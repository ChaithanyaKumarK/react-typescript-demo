import React, { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom'
import { connect, ConnectedProps } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit"
import { loginUser } from "../redux/actions/action_login_user";
import { resetVar } from "../redux/actions/action_login_user";
import "./loginPage.css"
import { RootState, AppDispatch } from "../redux/reducers/rootReducer";

const mapStateToProps = ({userDetails}: RootState) => ({
    userDetails
  })
  
  const mapDispatchToProps = (dispatch: AppDispatch) => {
      return bindActionCreators({
        loginUser,
        resetVar
      }, dispatch)
  
  }
  
  const connector = connect(mapStateToProps, mapDispatchToProps)
  
  type PropsFromRedux = ConnectedProps<typeof connector>
  
  interface Props extends PropsFromRedux {}

const LoginPage = (props :Props) => {
    const navigate = useNavigate();
    const [loginError, setloginError] = useState("");
    useEffect(() => {
        if (props.userDetails.error) {
            setloginError(props.userDetails.error_message);
        } else if (props.userDetails.post_login_success) {
            closeForm();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.userDetails.error, props.userDetails.post_login_success])


    const closeForm = () => {
        props.resetVar();
        navigate('/')
    }

    return (
        <>
            <div className=" d-flex justify-content-center align-items-center flex-column custom-login-form-box">
                <a href="/"><img className="mx-auto" src="/Wave_logo.svg" alt="Card" style={{ width: "150px" }} /></a>
                <h2>Sign in</h2>
                <form onSubmit={(e: React.SyntheticEvent) => {
                    e.preventDefault();
                    const target = e.target as typeof e.target & {
                        userName: { value: string };
                        password: { value: string };
                      };
                      const userName = target.userName.value; // typechecks!
                      const password = target.password.value; // typechecks!
                    const data = {userName,password};
                    props.loginUser(data);
                }}
                    className="custom-login-form">
                    <div className="form-group">
                        <input type="text" required className="form-control" id="userName" placeholder="Username" />
                    </div>
                    <div className="form-group">
                        <input type="password" required className="form-control" id="password" placeholder="Password" />
                    </div>
                    <label className="text-primary w-100">Forgot it?</label>
                    {loginError ? <label className="text-danger w-100">{loginError}</label> : <></>}
                    <button type="submit" className="btn btn-primary m-1  custom-login-button">Submit</button>
                    <button type="button" className="btn btn-secondary m-1 custom-login-button" onClick={closeForm}>Cancel</button>
                    <p>Don't have a Wave account yet? <a href="/signup">Sign up now.</a></p>
                </form>
            </div>
        </>
    )
}

export default connector(LoginPage)