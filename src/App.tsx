import './App.css';
import React from 'react';
import { Route, Routes } from "react-router-dom";
import LandingPage from './pages/LandingPage';
import NavBar from './components/navBar/Nav';
import Footer from './components/footer/Footer';
import LoginPage from './pages/LoginPage';
import SignUp from './pages/SignUpPage';

function App() {
  return (
    <>
      <NavBar />
      <Routes>
        <Route
          path="/"
          key="landingPage"
          element={<LandingPage/>}
        ></Route>
        <Route
          path="/login"
          key="loginPage"
          element={<LoginPage />}
        ></Route>
        <Route
          path="/signup"
          key="loginPage"
          element={<SignUp />}
        ></Route>

      </Routes>

      <Footer />
    </>
  );
}

export default App;
