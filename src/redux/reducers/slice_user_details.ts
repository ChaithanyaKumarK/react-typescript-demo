import { createSlice } from '@reduxjs/toolkit'

type Initial = {
    loading: boolean,
    currentUser: {
        userName?: string,
        password?: string
    },
    data: { userName: string, password: string, name?: string, email?: string }[],
    error_message: string,
    post_login_success: boolean,
    error: boolean
}

const initialState: Initial = {
    loading: true,
    currentUser: {},
    data: [{
        userName: "Chaithanya",
        password: "12345"
    }],
    error_message: "",
    post_login_success: false,
    error: false
}

export const userDetailsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        startLoginUser: (state) => {
            state.loading = true;
        },
        successLoginUser: (state, action) => {
            state.loading = false;
            state.error = false;
            state.error_message = "";
            state.currentUser = action.payload;
            state.post_login_success = true;
        },
        startInsertUserDetails: (state) => {
            state.loading = true;
        },
        successInsertUserDetails: (state, action) => {
            state.loading = false;
            state.error = false;
            state.data.push(action.payload);
            state.currentUser = action.payload;
        },
        error: (state, action) => {
            state.loading = false;
            state.error = true;
            state.error_message = action.payload;
        },
        reset: (state) => {
            state.post_login_success = false;
            state.error_message = "";
            state.error = false;
        },
        logout: (state) => {
            state.currentUser = {};
            state.error_message = "";
            state.post_login_success = false;
            state.error = false;
        }
    },
})

// Action creators are generated for each case reducer function
export const { startInsertUserDetails, successInsertUserDetails, startLoginUser, successLoginUser, error, reset, logout } = userDetailsSlice.actions

export default userDetailsSlice.reducer
