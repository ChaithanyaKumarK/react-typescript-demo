import "./subComponents.css"
import { Button } from '@mui/material';
import { useNavigate } from "react-router-dom";


const Hero = () => {
    const navigate = useNavigate();

    const toSignup = () => {
        navigate('/signup')
    }
    return (
        <>
            <div className="container-fluid outer-0">
                <div className="hero d-flex ">
                    <div className="box">
                        <h6>WAVE ACCOUNTING</h6>
                        <h1>Accounting software that's free and powerful</h1>
                        <p>
                            Wave's easy-to-use accounting software can connect your bank accounts, sync your expenses, balance your books, and get you ready for tax time. Start taking control of your finances today with the best accounting software for small businesses.
                        </p>
                        <Button
                            variant="contained"
                            onClick={toSignup}
                            sx={{
                                borderRadius: 5,
                                color: "black",
                                backgroundColor: "#FCB32C",
                                textTransform: "none",
                                fontSize: "1rem",
                                fontWeight: "600"
                            }}
                        >Create your free account</Button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Hero;