import "./signup.css"
import React, { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom'
import { connect, ConnectedProps } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit"
import { signUpUser } from "../redux/actions/action_signup_user"
import validator from 'validator';
import { RootState, AppDispatch } from "../redux/reducers/rootReducer";

const mapStateToProps = ({userDetails}: RootState) => ({
    userDetails
  })
  
  const mapDispatchToProps = (dispatch: AppDispatch) => {
      return bindActionCreators({
        signUpUser
      }, dispatch)
  
  }
  
  const connector = connect(mapStateToProps, mapDispatchToProps)
  
  type PropsFromRedux = ConnectedProps<typeof connector>
  
  interface Props extends PropsFromRedux {}

const SignUp = (props :Props) => {
    const [allUserNames, setAllUserNames] = useState([] as any)
    const [userName, setUserName] = useState("")
    const [isUserNameNonvalid, setIsUserNameNonvalid] = useState(false)
    const [name, setName] = useState("")
    const [isNameNonvalid, setIsNameNonvalid] = useState(false)
    const [email, setEmail] = useState("")
    const [isEmailNonvalid, setIsEmailNonvalid] = useState(false)
    const [password, setPassword] = useState("")
    const [isPasswordNonvalid, setIsPasswordNonvalid] = useState(false)
    const [rePassword, setRePassword] = useState("")
    const [isRePasswordNonvalid, setIsRePasswordNonvalid] = useState(false)
    const navigate = useNavigate();
    useEffect(() => {
        setAllUserNames(props.userDetails.data.map((eachdata:{ userName: string, password: string, name?: string, email?: string }) => eachdata.userName));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const closeForm = () => {
        navigate('/')

    }
    const isSubmitDisables = () => {
        return isUserNameNonvalid || isNameNonvalid || isEmailNonvalid || isPasswordNonvalid || isRePasswordNonvalid
    }

    const signupUser = () => {
        const data = {userName,name,email,password};
        props.signUpUser(data);
        navigate('/')
    }

    const handleOnChange = (e: React.FormEvent<HTMLInputElement>) => {
        switch (e.currentTarget.id) {
            case "email":
                setIsEmailNonvalid(!validator.isEmail(e.currentTarget.value))
                setEmail(e.currentTarget.value);
                break;
            case "userName":
                setIsUserNameNonvalid(allUserNames.includes(e.currentTarget.value))
                setUserName(e.currentTarget.value);
                break;
            case "name":
                setIsNameNonvalid(!validator.isAlpha(e.currentTarget.value))
                setName(e.currentTarget.value);
                break;
            case "password":
                setIsPasswordNonvalid(!validator.isStrongPassword(e.currentTarget.value))
                setPassword(e.currentTarget.value);
                break;
            case "repassword":
                setIsRePasswordNonvalid(!(password === e.currentTarget.value))
                setRePassword(e.currentTarget.value);
                break;
            default:
                break;
        }
    }
    return (<>
        <div className=" d-flex justify-content-center align-items-center flex-column custom-signup-form-box">
            <a href="/"><img className="mx-auto" src="/Wave_logo.svg" alt="Card" style={{ width: "150px" }} /></a>
            <h2>Sign up</h2>
            <form onSubmit={(e) => {
                e.preventDefault();
                signupUser();
            }}
                className="custom-signup-form">

                <div className="form-group">
                    <input type="text" required className={
                        isUserNameNonvalid
                            ? "form-control is-invalid"
                            : "form-control"
                    }
                        onChange={handleOnChange} id="userName" value={userName} placeholder="Username" />
                </div>
                <div className={isUserNameNonvalid ? "inline-errormsg" : "hidden"}>Username already exists</div>

                <div className="form-group">
                    <input type="text" required className={
                        isNameNonvalid
                            ? "form-control is-invalid"
                            : "form-control"
                    }
                        onChange={handleOnChange} id="name" value={name} placeholder="Name" />
                </div>
                <div className={isNameNonvalid ? "inline-errormsg" : "hidden"}>Enter a valid Name</div>

                <div className="form-group">
                    <input type="text" required className={
                        isEmailNonvalid
                            ? "form-control is-invalid"
                            : "form-control"
                    }
                        onChange={handleOnChange} id="email" value={email} placeholder="Email" />
                </div>
                <div className={isEmailNonvalid ? "inline-errormsg" : "hidden"}>Enter a valid Email</div>

                <div className="form-group">
                    <input type="password" required className={
                        isPasswordNonvalid
                            ? "form-control is-invalid"
                            : "form-control"
                    }
                        onChange={handleOnChange} id="password" value={password} placeholder="Password" />
                </div>
                <div className={isPasswordNonvalid ? "inline-errormsg" : "hidden"}>Enter a strong password</div>

                <div className="form-group">
                    <input type="password" required className={
                        isRePasswordNonvalid
                            ? "form-control is-invalid"
                            : "form-control"
                    }
                        onChange={handleOnChange} id="repassword" value={rePassword} placeholder="Re-Password" />
                </div>
                <div className={isRePasswordNonvalid ? "inline-errormsg" : "hidden"}>Passwords are not matching</div>

                <button type="submit" className="btn btn-primary m-1  custom-signup-button" disabled={isSubmitDisables()}>Submit</button>
                <button type="button" className="btn btn-secondary m-1 custom-signup-button" onClick={closeForm}>Cancel</button>

            </form>
        </div>

    </>)
}

export default connector(SignUp)