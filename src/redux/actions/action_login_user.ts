import { startLoginUser, successLoginUser, error, reset, logout } from "../reducers/slice_user_details"
import { AppDispatch, RootState } from "../reducers/rootReducer"


type SignInDataType = {
    userName:string,
    password:string
}

export const loginUser = (data: SignInDataType) => {
    return (dispatch:AppDispatch, getState:()=> RootState) => {
        dispatch(startLoginUser())
        const state = getState();
        let success = isDataPresent(state, data)
        if (success) {
            dispatch(successLoginUser(data))
        } else {
            dispatch(error("Login failed. Enter valid credentials."))
        }
    }
}
export const resetVar = () => {
    return (dispatch:AppDispatch) => {
        dispatch(reset())
    }
}
export const logoutUser = () => {
    return (dispatch:AppDispatch) => {
        dispatch(logout())
    }
}

const isDataPresent = (state: RootState, data: SignInDataType) => {
    let userFound = [];
    if (state.userDetails.data) {
        userFound = state.userDetails.data.filter((eachUser:{ userName: string, password: string, name?: string, email?: string }) => {
            if (eachUser.userName === data.userName && eachUser.password === data.password) {
                return true;
            }
            return false;
        })
    }
    if (userFound.length === 0) {
        return false;
    } else {
        return true;
    }
}