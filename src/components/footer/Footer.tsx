import "./footer.css";
import { Divider } from '@mui/material';

const Footer = () => {

    return (
        <>
            <Divider />
            <div className="container-fluid">
                <div className="d-flex align-items-stretch justify-content-around flex-wrap flex-md-nowrap footer">
                    <div className="list">
                        <ul className="list-group">
                            <li className="list-group-item header1">PRODUCTS</li>
                            <li className="list-group-item text-warp"><a href="/">Accounting Software</a></li>
                            <li className="list-group-item"><a href="/">Invoice Software</a></li>
                            <li className="list-group-item"><a href="/">Mobile Invoicing</a></li>
                        </ul>
                    </div>
                    <div className="list">
                        <ul className="list-group">
                            <li className="list-group-item header1">FEATURES</li>
                            <li className="list-group-item"><a href="/">Pricing - Free Software</a></li>
                            <li className="list-group-item"><a href="/">Recurring billing</a></li>
                            <li className="list-group-item"><a href="/">Invoice templates</a></li>
                            <li className="list-group-item"><a href="/">QuickBooks alternative</a></li>
                        </ul>
                    </div>
                    <div className="list">
                        <ul className="list-group">
                            <li className="list-group-item header1">WAVE</li>
                            <li className="list-group-item"><a href="/">About us</a></li>
                            <li className="list-group-item"><a href="/">Meet the team</a></li>
                            <li className="list-group-item"><a href="/">Careers</a></li>
                            <li className="list-group-item"><a href="/">Press</a></li>
                            <li className="list-group-item"><a href="/">Partners</a></li>
                            <li className="list-group-item"><a href="/">Sitemap</a></li>
                        </ul>
                    </div>
                    <div className="list">
                        <ul className="list-group">
                            <li className="list-group-item header1">RESOURCES</li>
                            <li className="list-group-item"><a href="/">Blog</a></li>
                            <li className="list-group-item"><a href="/">Help Center</a></li>
                            <li className="list-group-item"><a href="/">Community</a></li>
                            <li className="list-group-item"><a href="/">Freelance Hub</a></li>
                            <li className="list-group-item"><a href="/">Payments Report</a></li>

                        </ul>
                    </div>
                    <div className="list">
                        <ul className="list-group">
                            <li className="list-group-item header1">CONTACT US</li>
                            <li className="list-group-item"><a href="/">Contuct us</a></li>
                            <li className="list-group-item"><a href="/">System Status</a></li>
                        </ul>
                    </div>
                </div>
                <Divider />
                <div>
                    <p className=" footer-text text-center">
                        Copyright © 2010 - 2022 Wave Financial Inc. All Rights Reserved. Certain products and services offered in the United States are provided by Wave Financial USA Inc. NMLS#1846278<br />
                        Privacy Policy Terms of Use Security Licensing Accessibility
                    </p>
                </div>
            </div>
        </>)
}
export default Footer