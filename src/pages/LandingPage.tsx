import React from "react";
import Hero from "../components/LandingPageSubComponents/Hero"
import Subscriptions from "../components/LandingPageSubComponents/Subscriptions";
import CompaniesList from "../components/LandingPageSubComponents/CompaniesList";
import CustomerReview from "../components/LandingPageSubComponents/CustomerReview";
import { connect, ConnectedProps  } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit"
import { RootState, AppDispatch } from "../redux/reducers/rootReducer";


const mapStateToProps = (state: RootState) => ({
})

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return bindActionCreators({
    }, dispatch)

}

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>

interface Props extends PropsFromRedux {}

const LandingPage = (props: Props) => {
    return (
        <>
            <Hero />
            <Subscriptions />
            <CompaniesList />
            <CustomerReview />
        </>
    )
}


export default connector(LandingPage)