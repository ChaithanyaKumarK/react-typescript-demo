import "./nav.css";
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Button } from '@mui/material';
import { useNavigate } from "react-router-dom";
import { connect,ConnectedProps } from "react-redux";
import { RootState, AppDispatch } from "../../redux/reducers/rootReducer";
import { bindActionCreators } from "@reduxjs/toolkit"
import lodash from "lodash"
import { logoutUser } from "../../redux/actions/action_login_user"

const mapStateToProps = ({userDetails}: RootState) => ({
  userDetails
})

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return bindActionCreators({
      logoutUser
    }, dispatch)

}

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>

interface Props extends PropsFromRedux {}

function NavBar(props: Props) {
  const navigate = useNavigate();
  const toLogin = () => {
    navigate('/login')
  }
  return (
    <Navbar className="custom-padding" collapseOnSelect expand="sm" bg="dark" variant="dark">
      <Navbar.Brand href="/" className="custom-brandname"><img className="mx-auto" src="/companyLogo.png" alt="Card" style={{ width: "50px", height: "50px" }} />{" "}Custom</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" className="custom-button" />
      <Navbar.Collapse id="responsive-navbar-nav" className='custom-flex'>
        <Nav className="custom-child d-flex justify-items-end">
          <NavDropdown title="Products" id="collasible-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Accounting</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">
              Invoicing
            </NavDropdown.Item>
          </NavDropdown>
          <NavDropdown title="Services" id="collasible-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Support</NavDropdown.Item>
          </NavDropdown>
          <Nav.Link href="#features">Pricing</Nav.Link>
          <Nav.Link href="#pricing">Blog</Nav.Link>
          <NavDropdown title="More" id="collasible-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Success stories</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">
              About us
            </NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Careers</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.4">
              Partners
            </NavDropdown.Item>
            <NavDropdown.Item href="#action/3.4">
              Contact us
            </NavDropdown.Item>
          </NavDropdown>
          {lodash.isEmpty(props.userDetails.currentUser) ?
            <Nav.Link>
              <Button
                variant="contained"
                onClick={toLogin}
                sx={{
                  borderRadius: 5,
                  color: "black",
                  backgroundColor: "#FCB32C",
                  textTransform: "none",
                  fontSize: "0.8rem",
                  fontWeight: "600"
                }}
              >Login</Button>
            </Nav.Link>
            : <NavDropdown title={`Hi! ${props.userDetails.currentUser.userName}`} id="collasible-nav-dropdown">
              <NavDropdown.Item>
                <Button
                  variant="contained"
                  onClick={props.logoutUser}
                  sx={{
                    borderRadius: 5,
                    color: "black",
                    backgroundColor: "#FCB32C",
                    textTransform: "none",
                    fontSize: "0.8rem",
                    fontWeight: "600"
                  }}
                >Logout</Button>
              </NavDropdown.Item>
            </NavDropdown>}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default connector(NavBar); 