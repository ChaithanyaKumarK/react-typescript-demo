import { startInsertUserDetails, successInsertUserDetails } from "../reducers/slice_user_details"
import { AppDispatch } from "../reducers/rootReducer"

type SignUpDataType = {
    userName:string,
    name:string,
    email:string,
    password:string
}

export const signUpUser = (data: SignUpDataType) => {
    return (dispatch: AppDispatch) => {
        dispatch(startInsertUserDetails())
        dispatch(successInsertUserDetails(data))
    }
}